var data_import = [];
var radius = 0;

var path_data           = '';
var width_svg           = 600;
var height_svg          = 600;
var color_svg           = "rgba(255,255,255,0)";
var colours             = ["blue"];
var show_data           = true;
var radius_in           = 30;
var color_selected      = "SpringGreen";

//Init Template.
/*
* You can send empty param. So app get config to config.json.
* You can send this params:
*     iniciate(   <path_data>,                  --> String with path of file JSON with datas
*                 <width_svg>,                  --> Number with width of panel SVG
*                 <height_svg>,                 --> Number with height of panel SVG
*                 <color_svg>                   --> String with background-color of panel SVG
*                 <colours>,                    --> Array with colours for pies
*                 <show_data>,                  --> Boolean to show values in each pie.
*                 <radius_in>                   --> Number with inner radius of pie.
*                 <color_selected>)             --> Color to paint bar when it is selected.
*/
function iniciate(...params){

      //Load configuration
      if (params.length == 0){

            //get config data of config.json
            d3.json('config/config.json', function(err, data){
                  return data.json();
            }).then(function(data){
                  //Load configuration
                  load_config_json(data);
                  //Load data
                  load_data();
            });
      }else{
            //Load configuration
            load_config_array(params);
            //Load data
            load_data();
      };
};

//Load local variables from JSON.
function load_config_json(data){
      path_data         = data[0].path_data;
      width_svg         = data[0].width_svg;
      height_svg        = data[0].height_svg;
      color_svg         = data[0].color_svg;
      colours           = data[0].colours;
      show_data         = data[0].show_data;
      radius_in         = data[0].radius_in;
      color_selected    = data[0].color_selected;
}

function load_config_array(data){
      path_data         = data[0];
      width_svg         = data[1];
      height_svg        = data[2];
      color_svg         = data[3];
      colours           = data[4];
      show_data         = data[5];
      radius_in         = data[6];
      color_selected    = data[7];
}

//load data.
function load_data(){
      d3.json(path_data, function(err, data){
            return data.json();
      }).then(function(data){
            data_import = data;
            //Draw Bar Graphic
            draw_graphic();
      });
}

//Draw pie graphic
function draw_graphic(){

      //Radius
      radius = Math.min(width_svg, height_svg) / 2;

      //Arc
      var arc = d3.arc()
                  .outerRadius(radius - 20)
                  .innerRadius(radius_in);

      //Arc to transition
      var arcMove = d3.arc()
                      .outerRadius(radius)
                      .innerRadius(radius_in + 20);

      //Pie
      var pie = d3.pie()
                  .value(function(d){
                        return d.data;
                  });

      //SVG
      var svg = d3.select("body")
                  .append("svg")
                  .attr("height", height_svg)
                  .attr("width", width_svg)
                  .style("background-color", color_svg)
                  .append("g")
                  .attr("transform", "translate(" + width_svg/2 + ", " + height_svg/2 + ")");;

      //Insert acrs in SVG
      var g = svg.selectAll(".arc")
                  .data(pie(data_import))
                  .enter()
                  .append("g")
                  .attr("class", "arc");

      //Use path to draw arcs.
      g.append("path")
            .attr("d", arc)
            .style("fill", function(d, i){
                  return colours[i%colours.length];
            })
            .style("stroke", "#bbb")
            .on("mouseover", function(d){
                  d3.select(this)
                    .transition()
                    .duration(400)
                    .attr("d", arcMove);
            })
            .on("mouseout", function(d){
                  d3.select(this)
                    .transition()
                    .duration(400)
                    .attr("d", arc);
            });

      //Insert text with data in arcs of SVG
      if (show_data){
            g.append("text")
                  .text(function(d){
                        return d.data.info + " (" + d.data.data + ")";
                  })
                  .attr("transform", function(d){
                        var ang = angle(d);
                        var pos = arc.centroid(d);

                        if (ang < 180){
                              pos = [pos[0]/1.1, pos[1]/1.1];
                        }else{
                              pos = [pos[0]*1.25, pos[1]*1.25];
                        }

                        return "translate(" + pos + "), rotate(" + ang + ")"
                  });
      }
};

//Calculated angle for info
function angle(d){
      var angle = (d.startAngle + d.endAngle) * 90 / Math.PI;

      return angle > 180 ? angle+90 : angle-90;
}
